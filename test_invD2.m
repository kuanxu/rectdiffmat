% Test multiplication of rectangular diffmat for integrating exp(x).

clc, clear all, close all

%#ok<*SAGROW>
NN = round(2.^(4:0.125:11));
op = @(x) exp(x);

k = 0;
for N = NN

    disp(N)
    k = k + 1;

    % Chebyshev points:
    t = chebpts(N, 2); tau = chebpts(N - 1, 1);
    % Evaluate f and f':
    f = op(t); fp = op(tau);
    % BC and RHS:
    BC = eye(1, N);    
    rhs = [exp(-1) ; op(tau)];
      
    % Explicit:
    D = [BC ; rectdiff_exp(N)];     err_exp(k) = norm(D\rhs-f, inf);
    % Barycentric:
    D = [BC ; rectdiff_exp2(N)];    err_exp2(k) = norm(D\rhs-f, inf);    

end

%%

LW = 'LineWidth'; lw = 2;

loglog(NN, err_exp, '-r', NN, err_exp2, '-g', LW, lw), hold on
loglog(NN, eps*NN, '--k', 'LineWidth', 2); hold off
legend('explicit', 'explicit2', 'O(n)', 'location', 'nw');
axis([10, 2200, 1e-15, 1e-11])
set(gca, 'FontSize', 18), legend(gca, 'boxoff')

print -depsc test_invDf2
